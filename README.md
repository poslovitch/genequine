# Genequine

Explore the genealogy of horses.

<img src="https://upload.wikimedia.org/wikipedia/commons/a/ae/Wikidata_Stamp_Rec_Dark.svg" height=125px alt="Powered by Wikidata"/>

## Usage

## Hosting

Genequine can be hosted on Wikimedia Toolforge.

It requires a SQL database.

## Contributing
### Technologies

* Python 3.10
* Django 4.2
  * Django REST Framework

### Local development environment

1. Set up a `.venv`
2. Run `source .venv/bin/activate`
3. Run `pip install -r requirements.txt`
4. Run `python manage.py migrate`
5. Run `python manage.py runserver`
