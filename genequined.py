import math
from typing import List, Dict
from SPARQLWrapper import SPARQLWrapper, JSON
from collections import deque, namedtuple

UNKNOWN_HORSE = "-1"

Genealogy = List[int]
"""List of indices pointing to horses of the horses list (or -1 if an ancestor is unknown).
It represents a binary tree starting from the target horse and each layer of depth represents an older generation."""
CommonAncestor = namedtuple("CommonAncestor", ["horse_id", "sire_generation", "dam_generation"])


def generation(i: int) -> int:
    """
    Returns the generation of the provided index.
    @param i: index of a horse in the genealogy
    """
    return int(math.log2(i + 1))


def is_sire_ancestry(i: int) -> bool:
    """
    Returns true if this index is part of the sire's ancestry, false otherwise (dam's).
    @param i: index of a horse in the genealogy
    """
    # 0.5849 is the decimal value of log2 when exactly at the middle of a generation
    return abs(math.log2(i + 1) - int(math.log2(i + 1))) - 0.5849 < 0


def common_ancestors(genealogy: Genealogy) -> List[CommonAncestor]:
    sire_ancestry = [genealogy[i] for i in range(1, len(genealogy)) if is_sire_ancestry(i)]
    dam_ancestry = [genealogy[i] for i in range(1, len(genealogy)) if not is_sire_ancestry(i)]

    common_ancestors = list({a for a in sire_ancestry if a >= 0} & {a for a in dam_ancestry if a >= 0})

    result = [CommonAncestor(i, generation(sire_ancestry.index(i)) + 1, generation(dam_ancestry.index(i)) + 1) for i in
              common_ancestors]

    return result


def inbreeding_coefficient(genealogy: Genealogy) -> float:
    result = 0
    for common_ancestor in common_ancestors(genealogy):
        number_of_individuals = common_ancestor.sire_generation + common_ancestor.dam_generation
        result += math.pow(0.5, number_of_individuals - 1)
    return result


def build_genealogy(sparql_results, starter_horse: str, horses: Dict[str, int], max_gen: int = 16) -> Genealogy:
    genealogy = [horses[starter_horse]]
    pending = deque([starter_horse])

    counter = 0

    while pending and pending.count(UNKNOWN_HORSE) != len(pending):
        item = pending.popleft()

        if item == UNKNOWN_HORSE:
            genealogy.append(-1)
            genealogy.append(-1)
            pending.append(UNKNOWN_HORSE)
            pending.append(UNKNOWN_HORSE)
            continue

        counter = counter + 1
        if counter % 10 == 0:
            gen = generation(len(genealogy))
            print(f"Processed {counter} ancestors. [{len(pending)} pending|{'{:.2f}'.format(gen)}G]")
            if gen >= max_gen:
                break

        parents = [parent["item"]["value"] for parent in sparql_results if
                   "linkTo" in parent and parent["linkTo"]["value"] == item]
        if len(parents) == 0:
            genealogy.append(-1)
            genealogy.append(-1)
            pending.append(UNKNOWN_HORSE)
            pending.append(UNKNOWN_HORSE)

        elif len(parents) == 1:
            genealogy.append(horses[parents[0]])
            genealogy.append(-1)
            pending.append(parents[0])
            pending.append(UNKNOWN_HORSE)

        elif len(parents) == 2:
            genealogy.append(horses[parents[0]])
            genealogy.append(horses[parents[1]])
            pending.append(parents[0])
            pending.append(parents[1])

    return genealogy


def calculate_inbreeding(target):
    print(target)
    sparql = SPARQLWrapper("https://query.wikidata.org/bigdata/namespace/wdq/sparql",
                           agent="Toolforge:Genequine / User:Poslovitch")
    sparql.setReturnFormat(JSON)

    sparql.setQuery("""
    SELECT ?item ?itemLabel ?linkTo {
    ?item wdt:P40* wd:%s
    OPTIONAL { ?item wdt:P40 ?linkTo }
    SERVICE wikibase:label {bd:serviceParam wikibase:language "fr,en" }
    }
    """ % target)

    res = None
    try:
        res = sparql.queryAndConvert()["results"]["bindings"]
    except Exception as e:
        print(e)

    if res:
        target = "http://www.wikidata.org/entity/" + target

        horses = {item["item"]["value"]: i for i, item in enumerate(res)}
        labels = {item["item"]["value"]: item["itemLabel"]["value"] for item in res}

        # now build the genealogy
        genealogy = build_genealogy(res, target, horses)
        coefficient = inbreeding_coefficient(genealogy) * 100

        print(f"Étalon: {labels[target]}")
        print(f"Coefficient de consanguinité : {'{:.3f}%'.format(coefficient)}")
        return coefficient


if __name__ == "__main__":
    calculate_inbreeding(input("Entrez le Qid du cheval: "))
