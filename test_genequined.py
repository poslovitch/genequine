import pytest

from genequined import *


@pytest.mark.parametrize("gen,expected", [(0, 0), (1, 1), (16, 4), (24, 4)])
def test_generation(gen, expected):
    assert generation(gen) == expected


@pytest.mark.parametrize("horse,expected", [(1, True), (2, False), (14, False), (15, True), (22, True), (23, False)])
def test_is_sire_ancestry(horse, expected):
    assert is_sire_ancestry(horse) == expected


@pytest.mark.parametrize("genealogy, expected",
                         [
                             ([0, 1, -1, 2, 3], []),
                             ([0, 1, 2, 3, 4, 3, 5], [CommonAncestor(3, 2, 2)]),
                             ([0, 1, 2, 3, 2, 4, 5], [CommonAncestor(2, 2, 1)]),
                             ([0, 1, 2, 3, 2, 3, 4], [CommonAncestor(2, 2, 1), CommonAncestor(3, 2, 2)])
                         ])
def test_common_ancestors(genealogy, expected):
    assert common_ancestors(genealogy) == expected


@pytest.mark.parametrize("genealogy, expected",
                         [
                             ([0, 1, -1, 2, 3], 0),
                             ([0, 1, 2, 3, 4, 3, 5], 0.125),
                             ([0, 1, 2, 3, 2, 4, 5], 0.25),
                             ([0, 1, 2, 3, 2, 3, 4], 0.375)
                         ])
def test_inbreeding_coefficient(genealogy, expected):
    assert abs(inbreeding_coefficient(genealogy) - expected) < 0.001
