from django.db import models
from django.utils.timezone import now


class Horse(models.Model):
    qid = models.CharField(max_length=32, unique=True)
    inbreeding_coefficient = models.FloatField()
    calculation_timestamp = models.DateTimeField(default=now)

    def __str__(self):
        return f"Horse({self.qid})[inbreeding={self.inbreeding_coefficient},calculation_timestamp={self.calculation_timestamp}]"
