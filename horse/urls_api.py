from django.urls import path

from . import views

urlpatterns = [
    path("", views.HorsesListAPIView.as_view(), name="list-horses"),
    path("<str:horse_qid>", views.HorseAPIView.as_view(), name="horse")
]
