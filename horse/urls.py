from django.urls import path

from . import views

urlpatterns = [
    path("", views.index, name="index"),
    path("<str:horse_qid>", views.horse, name="horse")
]
