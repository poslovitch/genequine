from django.shortcuts import render
from django.http import HttpResponse
from datetime import datetime
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import HorseSerializer

from .models import Horse
from genequined import calculate_inbreeding


def index(request):
    return HttpResponse("Hello world!")


def horse(request, horse_qid):
    try:
        horse_object = Horse.objects.get(qid=horse_qid)
    except Horse.DoesNotExist:
        horse_object = Horse.objects.create(qid=horse_qid,
                                            inbreeding_coefficient=calculate_inbreeding(horse_qid),
                                            calculation_timestamp=datetime.strftime(datetime.utcnow(), "%Y-%m-%d %H:%M:%S"))

    context = {
        'horse': horse_object,
        'all_horses': Horse.objects.all()
    }
    return render(request, "horse/index.html", context)


class HorsesListAPIView(APIView):
    def get(self, request, *args, **kwargs):
        horses = Horse.objects.all()
        serializer = HorseSerializer(horses, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class HorseAPIView(APIView):
    def get(self, request, horse_qid, *args, **kwargs):
        """
        Return the horse for the given qid
        @param horse_qid:
        @param request:
        @param args:
        @param kwargs:
        @return:
        """
        try:
            horse_object = Horse.objects.get(qid=horse_qid)
        except Horse.DoesNotExist:
            horse_object = Horse.objects.create(qid=horse_qid,
                                                inbreeding_coefficient=calculate_inbreeding(horse_qid),
                                                calculation_timestamp=datetime.strftime(datetime.utcnow(),
                                                                                        "%Y-%m-%d %H:%M:%S"))

        serializer = HorseSerializer(horse_object)
        return Response(serializer.data, status=status.HTTP_200_OK)
