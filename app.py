# Required to run on Wikimedia Toolforge
import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "genequine.settings")

app = get_wsgi_application()
